@extends('layouts.app')

@section('title', $customer->first_name . "'s Order History")

@section('content')

<p>Live Time Value of Customer : ${{ $lifeTimeValue }}</p>
@if ($orders == null || count($orders) == 0)
	<p>Customer has never placed an order </p>
@else
 <table>
        <thead>
            <tr>
                <th>Date</th>
                <th># of Products</th>
                <th>Total</th>
	            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
            <tr>
       	    	<td>{{ $order->date_created }}</td> 
                <td>{{ count($order->products) }} </td>
                <td>${{ $order->total_inc_tax }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endif
@endsection

