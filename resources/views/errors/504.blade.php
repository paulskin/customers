@extends('errors::layout')

@section('title', 'Error')

@section('message', 'Sorry, we are having a temporary problem communicating to an upstream system.')
