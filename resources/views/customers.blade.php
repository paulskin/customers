@extends('layouts.app')

@section('title', 'Customers')

@section('content')
@if ($prevpage > 0)
 <a href="?page={{ $prevpage }} "><</a>
@else
<
@endif
   {{ $page }} of {{ $totalpages }}
@if ($nextpage > 0)
 <a href="?page={{ $nextpage }} ">></a>
@else
>
@endif
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th># of Orders</th>
		<th>View</th>
            </tr>
        </thead>
            <tbody>
                @foreach ($customers as $customer)
                    <tr>
                           <td>{{ $customer->first_name }} {{ $customer->last_name }}</td>
                           <td>{{ $customer->total_orders }}</td>
			   <td><a href="/customers/{{ $customer->id }}">View</a>
                    </tr>
                @endforeach
            </tbody>
    </table>

@endsection

