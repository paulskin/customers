<?php

/**
 * A set of unit tests to test the customerlist module
 *
 * PHP Version 7
 *
 * @category  PHP
 * @package   CustomersList
 * @author    Paul Skinner <paulskin@gmail.com>
 * @copyright 2018 Paul Skinner
 * @license   see license.txt
 * @link      https://bitbucket.org/paulskin/customers
 */

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;



/**
 * The testr case for testing the basics of the customer list
 *
 * These tests ensure that an error connecting to Big Commerce
 *  returns a 504 error otherwise it succeeds
 *
 * @category PHP
 * @package  CustomersList
 * @author   Paul Skinner <paulskin@gmail.com>
 * @license  see license.txt
 * @link     https://bitbucket.org/paulskin/customers
 */
class CustomersTest extends BigTestCase
{
    /**
     * A test to see if the page returns 200 when the API key is invalid
     *
     * @return void
     */
    public function testErrorHandling()
    {
    
        $apikey = env('API_KEY');
        try
        {
            putenv('API_KEY=123456');
            $this->setUp();
            $response = $this->get('/customers');
            $response->assertStatus(504);
        }
        finally
        {
            putenv('API_KEY='.$apikey);
        }

    }

    /**
     * A the happy path, i.e. when the API is configured correctly we get an http 200
     *
     * @return void
     */
    public function testSuccess()
    {
        $response = $this->get('/customers');
        $response->assertStatus(200);
    }

}
