<?php

/**
 * A base class for test cases that require the big commerce sdk
 *
 * PHP Version 7
 *
 * @category  PHP
 * @package   CustomersList
 * @author    Paul Skinner <paulskin@gmail.com>
 * @copyright 2018 Paul Skinner
 * @license   see license.txt
 * @link      https://bitbucket.org/paulskin/customers
 */

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Bigcommerce\Api\Client as Bigcommerce;

/**
 * A base test case that sets up the big commerce api each time a test is run
 *
 * The tests may reconfigure the Big Commerce SDK. This will make sure the tests
 *  can run in any order by reconfiguring the Big Commerce SDK each time
 *
 * @category PHP
 * @package  CustomersList
 * @author   Paul Skinner <paulskin@gmail.com>
 * @license  see license.txt
 * @link     https://bitbucket.org/paulskin/customers
 */

class BigTestCase extends TestCase
{
    /**
     * A setup function
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        Bigcommerce::configure(
            array(
            'store_url' => env('API_STORE_URL'),
            'username'  => env('API_USERNAME'),
            'api_key'   => env('API_KEY'),
            )
        );

	Bigcommerce::failOnError();    
    }
}

