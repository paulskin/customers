<?php

/**
 * A set of tests to test the customer details page
 *
 * PHP Version 7
 *
 * @category  PHP
 * @package   CustomersList
 * @author    Paul Skinner <paulskin@gmail.com>
 * @copyright 2018 Paul Skinner
 * @license   see license.txt
 * @link      https://bitbucket.org/paulskin/customers
 */



namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


/**
 * A set of test cases to test the Customer Details page
 *
 * These test cases validate the happy path as well as making sure
 *  the validation is handled correctly
 *
 * @category PHP
 * @package  CustomersList
 * @author   Paul Skinner <paulskin@gmail.com>
 * @license  see license.txt
 * @link     https://bitbucket.org/paulskin/customers
 */
class CustomerDetailsTest extends BigTestCase
{

    /**
     * Test the happy path, i.e. can we load a known customer and
     *  receive a http 200
     *
     * @return void
     */
    public function testSuccess()
    {
        $response = $this->get('/customers/10');
        $response->assertStatus(200);
    }

    /**
     * A test to make sure that if we pass something other than a 64 bit integer that
     * it will return a 404. This validates the internal validation on the route is working
     *  We would get an http 50x error if the id 100o was passed to the controller and on to
     *  the Big Commerce SDK
     *
     * @return void
     */
    public function testInvalidID()
    {    
        $response = $this->get('/customers/100o');
        $response->assertStatus(404);
        $response = $this->get('/customers/12345678901234567890');
        $response->assertStatus(404);
    }
}
