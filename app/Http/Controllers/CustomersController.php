<?php

/**
 * A controller to display a list of customers and their total orders
 *
 * PHP Version 7
 *
 * @category  PHP
 * @package   CustomersList
 * @author    Paul Skinner <paulskin@gmail.com>
 * @copyright 2018 Paul Skinner
 * @license   see license.txt
 * @link      https://bitbucket.org/paulskin/customers
 */


namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Bigcommerce\Api\Client as Bigcommerce;

/**
 * Controller class to display a list of customers and their total orders
 *
 * Returns a view that shows the customers and the total of their orders
 *
 * @category PHP
 * @package  CustomersList
 * @author   Paul Skinner <paulskin@gmail.com>
 * @license  see license.txt
 * @link     https://bitbucket.org/paulskin/customers
 */

class CustomersController extends BaseController
{

    /**
     * Shows a list of customers and their total number of orders
     *
     * @param Request $request The original HTTP request
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $page = $request->input('page');
        $total_customers = BigCommerce::getCustomersCount();
        $totalpages = (int)($total_customers / 10);

        if (! ($page > 0) || $page > $totalpages ) {
                $page = 1;
        }

        $filter = array("page" => (int)$page, "limit" => 10);
        $bigcustomers = BigCommerce::getCustomers($filter);

        // ideally we could get all orders for the customers on this page
        //  and count them. But I couldn't find a way to filter the orders
        //   by a list of customer_ids. 
        foreach ($bigcustomers as $bigcustomer) {
                $filter = array("customer_id" => (int)$bigcustomer->id);
                $bigcustomer->total_orders = BigCommerce::getOrdersCount($filter);
        }

        if ($page > 1) {
                $prevpage = $page - 1;
        } else {
                $prevpage = null;
        }

        if ($page  < $totalpages) {
                $nextpage = $page + 1;
        } else {
                $nextpage = null;
        }

        return view(
            'customers',
            ['customers' => $bigcustomers,
             'page' =>$page,
             'prevpage' => $prevpage,
             'nextpage' => $nextpage,
             'totalpages' => $totalpages]
        );
    }
}

