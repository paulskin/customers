<?php

/**
 * A controller to display customer order details
 *
 * PHP Version 7
 *
 * @category  PHP
 * @package   CustomersList
 * @author    Paul Skinner <paulskin@gmail.com>
 * @copyright 2018 Paul Skinner
 * @license   see license.txt
 * @link      https://bitbucket.org/paulskin/customers
 */


namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Bigcommerce\Api\Client as Bigcommerce;

/**
 * Controller class to display customer order details
 *
 * Displays the customer order summary and their life time value
 *
 * @category PHP
 * @package  CustomersList
 * @author   Paul Skinner <paulskin@gmail.com>
 * @license  see license.txt
 * @link     https://bitbucket.org/paulskin/customers
 */

class CustomerDetailsController extends BaseController
{
      
    /**
     * Show the order summary and life time value of a specific customer
     *
     * @param int $id The customer id of the customer as stored in bigcommerce
     * 
     * @return Response
     */
    public function show($id)
    {
        $bigcustomer = BigCommerce::getCustomer($id);
        if (!$bigcustomer) {
            abort(404);
        } else {
            $cust_filter = array('customer_id' => $id);
            $bigorders = BigCommerce::getOrders($cust_filter);
            if ($bigorders) {
                $lv = array_sum(array_column($bigorders, 'total_inc_tax'));
            } else {
                $lv = 0;
            }
        }

        return view(
            'details', [
            'customer' => $bigcustomer,
            'lifeTimeValue' => $lv,
            'orders' => $bigorders,
            'id' => $id,
            ]
        );
    }
}

