<?php

/**
 * The default exception handler
 *
 * PHP Version 7
 *
 * @category  PHP
 * @package   CustomersList
 * @author    Paul Skinner <paulskin@gmail.com>
 * @copyright 2018 Paul Skinner
 * @license   see license.txt
 * @link      https://bitbucket.org/paulskin/customers
 */

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Bigcommerce\Api\Client;
use Illuminate\Support\Facades\Log;

/**
 * The default exception handler
 *
 * This handler grabs any BigcommerceExceptions, logs them and returns a 504
 *
 * @category PHP
 * @package  CustomersList
 * @author   Paul Skinner <paulskin@gmail.com>
 * @license  see license.txt
 * @link     https://bitbucket.org/paulskin/customers
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception the exception to log
     *
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($exception instanceof \Bigcommerce\Api\Error) {
            Log::error(
                "Error communicating to bigcommerce. Error Code: " .
                $exception->getCode() . ". Message: " . $exception->getMessage()
            );
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request   incomming http request
     * @param \Exception               $exception the exception that was thrown
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Bigcommerce\Api\Error) {
            abort(504);
        }

        return parent::render($request, $exception);
    }
}
